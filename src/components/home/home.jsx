import { useEffect, useState } from 'react';
import { Header } from '../Navbar/Header';
import { Menu } from '../Menu/Menu';
import { Contents } from '../Menu/Contents';
import getComandsinfo from '../../services/getComandsInfo';

export function Home() {
  const [comandInfo, setComandInfo] = useState(null);
  useEffect(() => {
    getComandsinfo().then(setComandInfo);
  }, []);
  return (
    <div className='row mt-5'>
      {/* <Header /> */}
      {/* <Menu /> */}
      <div className='p-3 mx-auto'>
        <Contents comandInfo={comandInfo} />
      </div>
    </div>
  );
}
