export function StudentItem({
  id,
  nombre_completo,
  profesion,
  descripcion,
  correo,
}) {
  let imagenProfile = `../../src/assets/imgAbout/${id}.jpg`;
  return (
    <div className='d-flex col-12 py-4'>
      <div
        className='card col-12 mb-2 text-dark'
        style={{
          height: '400px',
          background: 'white',
          boxShadow: '5px 5px 10px #9E9E9E',
        }}
      >
        {/*Le da las propiedades al cuadro exterior*/}

        <div className='d-flex col-6'>
          <img
            src={imagenProfile}
            // cambiar "imagen" por {foto}
            className='img-fluid rounded-start'
            style={{
              marginLeft: '20px',
              marginTop: '-30px',
              height: '400px',
              width: '350px',
              objectFit: 'cover',
              boxShadow: '5px 5px 10px #9E9E9E',
            }}
            alt={nombre_completo}
          />
          {/*le da las propiedades a la foto*/}

          <div className='col-md-11'>
            <div className='card-body'>
              <h5 className='card-title'>{nombre_completo}</h5>
              <p className='text-muted'> {profesion}</p>
              <p className='mb-0 text' style={{ textAlign: 'justify' }}>
                {descripcion}
              </p>
              <p className='card-footer col align-self-end'>
                {' '}
                <a href={correo}>{correo}</a>
              </p>
            </div>
          </div>
          {/*Le da las propiedades al texto*/}
        </div>
      </div>
    </div>
  );
}
