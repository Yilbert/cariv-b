export function Menu() {
  return (
    <nav
      id="navbar-example3"
      className="navbar navbar-dark bg-dark flex-column align-items-stretch p-3"
    >
      <a className="navbar-brand" href="/">
        Introducción a Git
      </a>

      <a className="navbar-brand" href="/instalacion-git">
        Instalación de Git
      </a>

      <a className="navbar-brand" href="/quickstart">
        Quickstart Git y Trabajando en paralelo
      </a>

      <a className="navbar-brand" href="/intro-gitlab">
        Introducción a GitLab
      </a>

      <a className="navbar-brand" href="/profundizando">
        Profundizando en Git
      </a>
    </nav>
  );
}
