export function IntroGitlab() {
  return (
    <div className="container my-5 py-5">
      <h1 className="text-center">Introducción a GitLab</h1>
      <div className="pt-3">
        <p className="text-justify">
          GitLab es un popular sistema de control de versiones (VCS) que se usa,
          sobre todo, en el ámbito del desarrollo de software. En el año 2011,
          Dimitri Saparoschez escribió y publicó este software, basado en la
          web, en el lenguaje Ruby on Rails. Hoy en día, se ha convertido en
          imprescindible para los desarrolladores.
        </p>
        <p className="text-justify">
          La ventaja principal de GitLab es que facilita notablemente el
          desarrollo de software ágil entre varios equipos. De esta manera,
          varios desarrolladores pueden trabajar simultáneamente en un proyecto
          y editar, por ejemplo, diferentes funciones de forma paralela. La
          protocolización continua de todos los procesos garantiza que no se
          pierda ninguna modificación del código ni que se sobrescriba de forma
          no intencionada. También es posible deshacer rápidamente los cambios
          ya aplicados.
        </p>
        <p id="item-3-1" className="text-justify">
          GitLab se basa en el software de gestión de versiones de uso común
          Git. Git está disponible de forma gratuita como software de código
          abierto y es considerado uno de los sistemas de control de versiones
          más usados en general. GitLab es una de las alternativas a GitHub más
          populares (cuando Microsoft se hizo con GitHub en 2018, muchos
          usuarios se cambiaron a GitLab).
        </p>

        <h4 className="mt-0 mb-4 text-center">¿Cómo funciona?</h4>
        <p>
          GitLab es una aplicación basada en la web con una interfaz gráfica de
          usuario que también puede instalarse en un servidor propio. El núcleo
          de GitLab lo forman los proyectos en los que se guarda el código que
          va a editarse en archivos digitales, los denominados repositorios. En
          estos directorios de proyecto se encuentran todos los contenidos y
          archivos de un proyecto de software, es decir, archivos JavaScript,
          HTML, CSS o PHP, entre otros.
        </p>
        <p>
          En este tutorial de GitLab te explicamos cómo funciona. Para comenzar,
          todos los implicados en el proyecto se descargan una copia propia del
          repositorio central en su ordenador. A partir de ahí, los cambios del
          código se realizan mediante denominados commits. Una vez realizada la
          edición, los cambios se integran en el repositorio principal.
        </p>
        <p>
          Otra función importante es la ramificación. Esta permite a los
          usuarios crear una “rama” que se bifurca de la parte principal del
          código y que se puede editar de forma independiente a este. Esta
          función es especialmente útil a la hora de introducir y probar nuevas
          funciones sin que esto afecte al desarrollo de la línea principal.
        </p>
        <p>
          Gracias a la entrega e integración continuas integradas, GitLab es
          perfecto para trabajar con ramificación y ofrece funciones muy útiles
          como solicitudes de combinación y la creación de bifurcaciones. Por
          ello, el software es una de las herramientas de integración continua
          más populares.
        </p>

        <h4 id="item-3-2" className="text-center py-3">
          Vista general de sus funciones
        </h4>
        <p>
          Entre las funciones más importantes de GitLab se encuentran las
          siguientes:
        </p>
        <div className="ms-3">
          <p>➤ Interfaz fácil de usar</p>
          <p>➤ Las ramificaciones pueden permanecer privadas o publicarse</p>
          <p>➤ Posibilidad de gestionar varios repositorios</p>
          <p>➤ Revisión de códigos</p>
          <p>➤ Localización integrada de errores y problemas</p>
          <p>
            ➤ Integración continua y entrega continua (CI/CD, por sus siglas en
            inglés) integradas de forma gratuita
          </p>
          <p>➤ Wikis de proyectos</p>
          <p>
            ➤ Creación sencilla de fragmentos de código para dividir partes del
            código.
          </p>
        </div>
        <h4 id="item-3-3" className="text-center py-3">
          Beneficios de Gitlab
        </h4>
        <div className="ms-3">
          <p>
            ➤ GitLab es fácil de configurar y administrar con sus códigos
            disponibles gratuitamente.
          </p>
          <p>
            ➤ La aplicación única es única y crea un flujo de trabajo optimizado
            con colaboración y eficiencia.
          </p>
          <p>
            ➤ Las revisiones de código junto con las solicitudes de extracción
            son fáciles de usar y compactas.
          </p>
          <p>
            ➤ Al ser una aplicación nativa de la nube con fuertes medidas de
            seguridad, ofrece características de seguridad como restricciones
            granulares, autenticación de usuario con Kerberos.
          </p>
          <p>
            ➤ Integración mínima para reducir el ciclo de vida del desarrollo
            mientras aumenta la productividad
          </p>
          <p>
            ➤ Facilita una organización adecuada de Kubernetes y la integración
          </p>
          <p>
            ➤ Permite una gestión de proyectos amplia y adaptable para acelerar
            el flujo de trabajo
          </p>
        </div>
      </div>
    </div>
  );
}
