import repository from "../utils/repository";

export const getComandsinfo = async () => {
  let res = await repository.get(`/comand`);
  return res.data.comandos;
};

export default getComandsinfo;
